CREATE TABLE contract_service_office(
    id BIGINT AUTO_INCREMENT NOT NULL,
    office_id BIGINT NOT NULL,
    contract_id BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (office_id) REFERENCES office(id),
    FOREIGN KEY (contract_id) REFERENCES contract(id)
);