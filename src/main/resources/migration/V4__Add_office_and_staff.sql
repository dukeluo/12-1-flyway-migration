CREATE TABLE office(
    id BIGINT AUTO_INCREMENT NOT NULL,
    country VARCHAR(32) NOT NULL,
    city VARCHAR(32) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE staff(
    id BIGINT AUTO_INCREMENT NOT NULL,
    office_id BIGINT NOT NULL,
    first_name VARCHAR(32) NOT NULL,
    last_name VARCHAR(32) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (office_id) REFERENCES office(id)
);