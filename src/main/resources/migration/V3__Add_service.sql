CREATE TABLE service(
    id BIGINT AUTO_INCREMENT NOT NULL,
    description VARCHAR(32) NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE contract ADD service_id BIGINT NOT NULL;
ALTER TABLE contract ADD FOREIGN KEY (service_id) REFERENCES service(id);
