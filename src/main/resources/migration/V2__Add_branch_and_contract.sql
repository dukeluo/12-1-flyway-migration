CREATE TABLE branch (
    id BIGINT AUTO_INCREMENT NOT NULL,
    client_id BIGINT NOT NULL,
    name VARCHAR(32) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (client_id) REFERENCES client(id)
);

CREATE TABLE contract (
    id BIGINT AUTO_INCREMENT NOT NULL,
    branch_id BIGINT NOT NULL,
    name VARCHAR(32) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (branch_id) REFERENCES branch(id)
);