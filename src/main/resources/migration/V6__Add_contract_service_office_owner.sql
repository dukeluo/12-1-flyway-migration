ALTER TABLE contract_service_office ADD staff_id BIGINT NOT NULL;
ALTER TABLE contract_service_office ADD FOREIGN KEY (staff_id) REFERENCES staff(id);